import os
import torch
import requests
import time
import datetime
import json
import logging
from ftplib import FTP
from pathlib import Path
from dotenv import load_dotenv
import os
load_dotenv()
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

ACTIVATE_URL = os.getenv('ACTIVATE_URL', 'https://rpi.example.net:8080/on')
DEACTIVATE_URL = os.getenv('DEACTIVATE_URL', 'https://rpi.example.net:8080/off')
CAM_URL = os.getenv('CAM_URL', 'https://cam.example.net/cgi-bin/api.cgi?cmd=Snap&channel=0C&user=user&password=password')  # Reolink example
SAVE_DIR = os.getenv('SAVE_DIR', None)
FTP_PATH = os.getenv('FTP_PATH', None)
FTP_HOST = os.getenv('FTP_HOST', None)
FTP_USER = os.getenv('FTP_USER', None)
FTP_PASSWORD = os.getenv('FTP_PASSWORD', None)
ITERTIME = os.getenv('ITERTIME', 2)

logger.info('Starting ...')

whitelist = ['person']

model = torch.hub.load('ultralytics/yolov5', 'yolov5m6')
model.conf = 0.4
model.classes = [0, 15, 16, 18]  # None : (optional list) filter by class, i.e. = [0, 15, 16] for persons, cats and dogs

logger.info('Observing ...')

while True:

    try:
        # Detect objects in Image
        results = model(CAM_URL)
        obj_lst = results.pandas().xyxy[0].to_dict(orient="records")

        for obj in obj_lst:
            logger.info(obj)
            if obj['name'] in whitelist:
                logger.info('{} detected, sleeping for {} seconds'.format(obj['name'], ITERTIME*9))
                # Deactivate the sprinkler
                requests.get(DEACTIVATE_URL)
                time.sleep(ITERTIME*9)
                pass
            else:
                # Activate the sprinkler
                requests.get(ACTIVATE_URL)

                filename = '{}.jpg'.format(datetime.datetime.now())
                results.files[0] = filename
                file = '{}/{}'.format(SAVE_DIR, filename)

                if FTP_HOST or SAVE_DIR:
                    if FTP_HOST and not SAVE_DIR:
                        SAVE_DIR = './tmp_img'
                    results.save(SAVE_DIR)
                if FTP_HOST:
                    with FTP(FTP_HOST, FTP_USER, FTP_PASSWORD) as ftp, open(file, 'rb') as f:
                        if FTP_PATH:
                            ftp.cwd(FTP_PATH)
                        ftp.storbinary(f'STOR {filename}', f)
                    Path(file).unlink()

    except Exception as e:
        logger.warning('Something went wrong, retrying in {} seconds'.format(ITERTIME))
        logger.warning(e)
    
    time.sleep(ITERTIME)
